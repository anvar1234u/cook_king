﻿using System;
using System.Collections;
using System.Collections.Generic;
using CookKing.MVC.Controller;
using CookKing.MVC.Model;
using CookKing.MVC.View;
using CookKing.Utils;
using UnityEngine;
namespace CookKing.UI
{
    public class CustomerSlotManager : MonoBehaviour
    {
        #region Inspector_fields
        [SerializeField] private List<CustomerController> customers;
        #endregion

        public static CustomerSlotManager instance;
        List<int> navigatioCount = new List<int> {0,0};
        List<CustomerController> waitingCustomers;
        internal Action<int, CustomerController> OnSelectCustomer;

        private void Awake()
        {
            instance = this;
        }

        public void StartVisiting()
        {
            InvokeRepeating("CustomerVisit", 1, ConstantStorage.customerVisitInterval);
        }

        /// <summary>
        /// a new Customer (if any) will visit in every 5 seconds
        /// </summary>
        void CustomerVisit()
        {
            var customerToVisit = customers.Find(X => X.GiveMeCustomerState() == CustomerState.IDLE);
            if (customerToVisit != null)
            {
                customerToVisit.OnCustomerVisit();
                waitingCustomers = customers.FindAll(x => x.GiveMeCustomerState() == CustomerState.WAITING);
                if (waitingCustomers.Count == 1)
                {
                    UpdateNavigation();
                }
            }
        }

        /// <summary>
        /// handles the players input based on player id (0 and 1)
        /// </summary>
        /// <param name="action"></param>
        /// <param name="playerId"></param>
        internal void PlayerInput(Actions action,int playerId)
        {
            waitingCustomers = customers.FindAll(x => x.GiveMeCustomerState() == CustomerState.WAITING);
            if (waitingCustomers == null || waitingCustomers.Count <= 0)
            {
                return;
            }
            var numberOfWaitingCustomers = waitingCustomers.Count;
            int tempVal = 0;
            switch (action)
            {
                case Actions.MOVE_LEFT:
                    tempVal = navigatioCount[playerId];
                    tempVal = tempVal <= 0 ? numberOfWaitingCustomers-1 : tempVal-1;
                    navigatioCount[playerId] = tempVal;
                    UpdateNavigation();
                    break;
                case Actions.MOVE_RIGHT:
                    tempVal = navigatioCount[playerId];
                    tempVal = tempVal >= numberOfWaitingCustomers-1 ? 0 : tempVal + 1;
                    navigatioCount[playerId] = tempVal;
                    UpdateNavigation();
                    break;
                case Actions.SELECT:
                    UpdateSelection(playerId);
                    break;
                    
            }
            
        }

        /// <summary>
        /// Navigate throug the customer slots based on inputs
        /// </summary>
        void UpdateNavigation()
        {
            foreach (CustomerController customer in customers)
            {
                if (customer.GiveMeCustomerState() == CustomerState.WAITING)
                {
                    customer._view.ResetNavigation();
                }
            }
            var indexPlayer1 = navigatioCount[0];
            var indexPlayer2 = navigatioCount[1];

            if (indexPlayer1 >= 0)
            {
                transform.GetChild(indexPlayer1).GetComponent<CustomerController>()._view.Navigate(0, indexPlayer1 == indexPlayer2);
                //customers[indexPlayer1].Navigate(0, indexPlayer1 == indexPlayer2);
            }
            if (indexPlayer2 >= 0)
            {
                transform.GetChild(indexPlayer2).GetComponent<CustomerController>()._view.Navigate(1, indexPlayer1 == indexPlayer2);
                //customers[indexPlayer2].Navigate(1, indexPlayer1 == indexPlayer2);
            }
        }

        /// <summary>
        /// On player confroms an order
        /// </summary>
        /// <param name="playerId"></param>
        void UpdateSelection(int playerId)
        {
            var index = navigatioCount[playerId];
            var customer = transform.GetChild(index).GetComponent<CustomerController>();
            if (customer.GiveMeCustomerState() == CustomerState.OCCUPIED) return;// this check required, both player selecting same customer at same time
            customer.transform.SetAsLastSibling();
            navigatioCount[playerId] =-1;
            customer.AttendThisCustomer(playerId);
            if (waitingCustomers.Count > 1)
            {
                UpdateNavigation();
            }
            OnSelectCustomer?.Invoke(playerId, customer);
        }

        public void PlayerCompletesTheOrder(int playerId)
        {
            navigatioCount[playerId] = 0;
            foreach(Transform t in transform)
            {
                if (t.GetComponent<CustomerController>().GiveMeCustomerState() == CustomerState.OCCUPIED)
                {
                    t.SetAsLastSibling();
                    break;
                }
            }
            UpdateNavigation();
        }

        public void Reset()
        {
            foreach (CustomerController controller in customers)
            {
                controller.Reset();
                controller.transform.SetAsLastSibling();
            }
            UpdateNavigation();
            waitingCustomers.Clear();
            CancelInvoke("CustomerVisit");
            navigatioCount[0] = 0;
            navigatioCount[1] = 0;
        }

    }
}
