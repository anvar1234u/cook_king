﻿using System.Collections;
using System.Collections.Generic;
using CookKing.MVC.Model;
using UnityEngine;
using UnityEngine.UI;
namespace CookKing.MVC.View
{
    public class OrderView : MonoBehaviour
    {
        public Image itemIcon;
        public Text itemCount;
        /// <summary>
        /// Setting up the order view
        /// </summary>
        /// <param name="order"></param>
        public void SetView(Order order)
        {
            itemIcon.sprite = order.item.icon;
            itemCount.text = order.quantity.ToString();
            gameObject.SetActive(true);
        }

        public void Reset()
        {
            gameObject.SetActive(false);
        }

    }
}