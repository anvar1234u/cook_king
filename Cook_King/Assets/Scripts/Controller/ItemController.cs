﻿using System.Collections;
using System.Collections.Generic;
using CookKing.MVC.Model;
using CookKing.MVC.View;
using UnityEngine;
namespace CookKing.MVC.Controller
{
    public class ItemController : MonoBehaviour
    {
        public Item model;
        public ItemView view;
        public void Init(Item _model)
        {
            model = _model;
            view.Init(_model);
        }
    }
}
