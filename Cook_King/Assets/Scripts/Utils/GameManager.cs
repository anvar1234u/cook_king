﻿using System.Collections;
using System.Collections.Generic;
using CookKing.MVC.Controller;
using CookKing.MVC.Model;
using CookKing.MVC.View;
using CookKing.UI;
using UnityEngine;
namespace CookKing.Utils
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager instance;

        public List<PlayerController> players;
        public List<Item> allItems;
        
        public int totalGameTime;
        public System.Action<string> onTimerUpdated;

        private void Awake()
        {
            instance = this;
        }

        private void Start()
        {
            //PlayGame();
        }

        /// <summary>
        /// Start the game
        /// </summary>
        public void PlayGame()
        {
            UIManager.instance.gamePlayScreen.SetActive(true);
            StartCoroutine("RunPlayerTimer");
        }

        public Item GiveMeRandomItem(Customer customer)
        {
            List<Item> tempList = new List<Item>(allItems);

            foreach (Order order in customer.orders)
            {
                if (tempList.Count <= 0)
                {
                    return null;
                }
                if (tempList.Contains(order.item))
                {
                    //Debug.Log("Removing already selected item!!");
                    tempList.Remove(order.item);
                }
            }
            int index = Random.Range(0, tempList.Count);
            return tempList[index];
        }

        public string GiveTimeString(int seconds)
        {

            int minute = seconds / 60;
            int sec = seconds % 60;
            string timeString = "";

            if (minute < 10)
            {
                timeString += "0" + minute + ":";
            }
            else
            {
                timeString += minute + ":";
            }
            if (sec < 10)
            {
                timeString += "0" + sec;
            }
            else
            {
                timeString += sec;
            }
            return timeString;
        }

        IEnumerator RunPlayerTimer()
        {
            CustomerSlotManager.instance.StartVisiting();
            var tick = new WaitForSeconds(1);
            var tempTime = totalGameTime;
            while (tempTime > 0)
            {
                onTimerUpdated?.Invoke("Time : " + GiveTimeString(tempTime));
                tempTime--;
                yield return tick;
            }
            //Time over Game over find the winner
            FindWinner();
            UIManager.instance.resultScreen.Activate();
        }

        void FindWinner()
        {
            var player1 = players[0];
            var player2 = players[1];

            if (player1._model.score > player2._model.score)
            {
                player1._model.result = Result.WIN;
                player2._model.result = Result.LOSE;
            }
            else if (player2._model.score > player1._model.score)
            {
                player1._model.result = Result.LOSE;
                player2._model.result = Result.WIN;
            }
            else
            {
                player1._model.result = Result.TIE;
                player2._model.result = Result.TIE;
            }

        }
        

    }
}
