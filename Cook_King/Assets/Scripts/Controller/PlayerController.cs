﻿using System;
using System.Collections;
using System.Collections.Generic;
using CookKing.MVC.Model;
using CookKing.MVC.View;
using CookKing.UI;
using CookKing.Utils;
using UnityEngine;
namespace CookKing.MVC.Controller
{
    public class PlayerController : MonoBehaviour
    {
        #region inspector_fields
        public Player _model;
        public PlayerView _view;
        [SerializeField]private InventoryManager inventory;
        [SerializeField]private CuttingBoardManager cuttingBoard;
        #endregion

        private CustomerController servingCustomer;

        #region MonobehaviourMethods
        private void Start()
        {
            InitializeControls();
            InputController.onKeyPressed += PlayerInput;
            CustomerSlotManager.instance.OnSelectCustomer += OnSelectCustomer;
            cuttingBoard.OnPreperationCompleted += OnFinishPreperation;
            inventory.onSelectItem += OnSelectItem;
            _view.InitView(_model);
        }
        #endregion

       
        /// <summary>
        /// Add the inputs into a dictionary for easy access
        /// </summary>
        void InitializeControls()
        {
            foreach (PlayerInputKeys keys in _model.playerInputKeys)
            {
                if (!_model.inputKeys.ContainsKey(keys.key.ToUpper()))
                {
                    _model.inputKeys.Add(keys.key.ToUpper(), keys.action);
                }
            }
        }

        private void PlayerInput(string inputKey)
        {
            if (_view.isCustomerIsThanking) return;//wait for customer to leave
            if (_model.inputKeys.ContainsKey(inputKey))
            {
                var action = _model.inputKeys[inputKey];
                if (_model.isSelectedCustomerPanel)// selecting customers
                {
                    CustomerSlotManager.instance.PlayerInput(action, _model.playerId);
                }
                else
                {
                    inventory.PlayerInput(action);
                }

            }
          
        }

        void OnSelectCustomer(int _playerId, CustomerController customer)
        {
            if (_playerId != _model.playerId)
            {
                return;
            }
            servingCustomer = customer;
            _view.OnSelectCustomer(servingCustomer.model);
            _model.isSelectedCustomerPanel = false;
            _model.totalOrderAttended++;
            inventory.UpdateNavigation();
        }

        public void OnCompletsOrder(Customer customer,bool isSuccess)
        {
            CustomerSlotManager.instance.PlayerCompletesTheOrder(_model.playerId);
            _view.ShowSpeechBubbleOnOrderFinish(isSuccess);
            cuttingBoard.Reset();
            inventory.Reset();
            //if (isSuccess)
            { 
                CalculateScore(customer,isSuccess);
            }
        }

        void CalculateScore(Customer customer,bool isSuccess)
        {
            var score = _model.score;
            foreach (Order order in customer.orders)
            {
                if (isSuccess)
                {
                    score += order.quantity * order.item.preperationTime;// normal score for completion
                }
                else
                {
                    score += (order.quantity - order.quantityRecieved) * order.item.preperationTime;// used for deducting the score,
                                                                                                    //calculated using remaining amount of items to serve.
                }
            }
            if (isSuccess)
            {
                //for success order adds the score 
                score += customer.waitingTime;//remaining time bonus if success
                _model.orderCompleted++;
                _model.score += score;
            }
            else
            {
                //for failour order deducts the score (no negative score)
                _model.score -= score;
                _model.orderFailed++;
                if (_model.score < 0)
                {
                    _model.score = 0;
                }
            }
            _view.UpdateScoreView();

        }
        void OnSelectItem(Item item)
        {
            //Debug.Log(_model.playerName + " Selected " + item.itemName);
            //if (queueCount <= 1)
            {
                cuttingBoard.PlaceTheItemOnBoard(item);
            }
            //if (queueCount < 1)
            //{
            //    queueCount++;
            //}
        }

        /// <summary>
        /// On finish the item preperation
        /// </summary>
        /// <param name="item"></param>
        /// <param name="numberOfItemInQueue"></param>
        void OnFinishPreperation(Item item,int numberOfItemInQueue)
        {
            //queueCount = numberOfItemInQueue >0? numberOfItemInQueue-1 : 0;
            _model.totalItemsChopped++;
            servingCustomer.OnItemPrepared(item, OnCorrectItemPlaced, OnWrongItemPlaced);
        }
        /// <summary>
        /// When a wrong item placed need do necessory calculation in score
        /// </summary>
        /// <param name="item">wrongly placed item</param>
        void OnWrongItemPlaced(Item item)
        {
            _model.itemTrashed++;
            _view.SetSpeechBubble("I didn't ask for that!");
            cuttingBoard.TrashTheItem(item);

        }

        void OnCorrectItemPlaced(int orderIndex,Order order)
        {
            _model.itemServed++;
            _view.SetPlateView(orderIndex, order);
        }

        private void OnDestroy()
        {
            InputController.onKeyPressed -= PlayerInput;
            CustomerSlotManager.instance.OnSelectCustomer -= OnSelectCustomer;
            cuttingBoard.OnPreperationCompleted -= OnFinishPreperation;
            inventory.onSelectItem -= OnSelectItem;
        }

        /// <summary>
        /// Called only when play again button is clicked
        /// </summary>
        public void Reset()
        {
            _view.ResetPlateView();
            _model.Reset();
            _view.UpdateScoreView();
            cuttingBoard.Reset();
            inventory.Reset();
        }

    }
}
