﻿using System.Collections;
using System.Collections.Generic;
using CookKing.MVC.Model;
using CookKing.MVC.View;
using CookKing.Utils;
using UnityEngine;
namespace CookKing.MVC.Controller
{
    public class CustomerController : MonoBehaviour
    {
        #region InspectorFields
        public Customer model;
        public CustomerView _view;
        #endregion

        private int waitTimeBuffer = 20;
        private bool isOrderSuccess = false;
        private int maxNumberOfOrders = 2;
        private int maxNumberOfItems = 2;

        #region MonobehaviourClasses
        private void OnDestroy()
        {
            Reset();
        }
        #endregion

        public void OnCustomerVisit()
        {
            gameObject.SetActive(true);
            CreateOrder();
        }

        public Sprite GetCustomerAvatar()
        {
            return model.avatar;
        }
        /// <summary>
        /// Creates a customer visit
        /// </summary>
        public void CreateOrder()
        {
            int numberOfOrder = Random.Range(1, maxNumberOfOrders);
            int totalWaitingTime = waitTimeBuffer;
            isOrderSuccess = false;
            model.orders.Clear();
            for (int i = 0; i < numberOfOrder; i++)
            {
                Order order = new Order();
                order.item = GameManager.instance.GiveMeRandomItem(model);
                order.quantity = Random.Range(1, maxNumberOfItems);
                totalWaitingTime += order.item.preperationTime * order.quantity;
                //if(order.item != null)
                    model.orders.Add(order);
            }
            model.waitingTime = totalWaitingTime;
            model.customerState = CustomerState.WAITING;
            _view.SetView(model);
            maxNumberOfOrders++;
            maxNumberOfItems++;
            if (maxNumberOfItems > 6)
            {
                maxNumberOfItems = 6;
            }
            if (maxNumberOfOrders > 4)
            {
                maxNumberOfOrders = 4;
            }
        }

        /// <summary>
        /// a customer's order is attended by a player
        /// </summary>
        /// <param name="attender"> is the player id who conformed the order</param>
        public void AttendThisCustomer(int attender)
        {
            model.customerState = CustomerState.OCCUPIED;
            model.occupiedPlayerId = attender;
            _view.OccupyThisCustomer();
            StopAllCoroutines();
            StartCoroutine(RunTimer(model.waitingTime/*,false*/));
        }

        /// <summary>
        /// Run timer for the customer once a player conforms the order.
        /// </summary>
        /// <returns></returns>
        IEnumerator RunTimer(int totalWaitingTime/*,bool isWaitingToAttend*/)
        {
            float tempWaitTime = totalWaitingTime;//customerModel.waitingTime;
            while (tempWaitTime > 0)
            {
                var deltaTime = Time.deltaTime;
                tempWaitTime -= deltaTime;
                _view.SetTimeSlider((float)(tempWaitTime / totalWaitingTime));
                if (isOrderSuccess)
                {
                    model.waitingTime = (int)tempWaitTime;// storing remaining time for bonus
                    break;
                }
                //if (customerModel.customerState != CustomerState.WAITING && isWaitingToAttend)
                //{
                //    break;
                //}
                yield return new WaitForSeconds(deltaTime);
            }
            if (!isOrderSuccess)// on order time out
            {
                model.customerState = CustomerState.IDLE;
                GameManager.instance.players[model.occupiedPlayerId].OnCompletsOrder(model,false);
                Reset();
            }
            isOrderSuccess = false;
        }


        /// <summary>
        /// calls when order time out or player completes the order, (On customer leaves we need to reset)
        /// </summary>
        public void Reset()
        {
            StopAllCoroutines();
            isOrderSuccess = false;
            gameObject.SetActive(false);
            maxNumberOfItems = 2;
            maxNumberOfOrders = 2;
            _view.ResetView();
            model.waitingTime = 0;
            model.customerState = CustomerState.IDLE;
            model.occupiedPlayerId = -1;
        }

        /// <summary>
        /// a player completes preperation an item
        /// </summary>
        /// <param name="item"></param>
        public void OnItemPrepared(Item item, System.Action<int,Order> onCorrectItemPlaced, System.Action<Item> onWrongItemPlaced)
        {
            var orderToUpdate = model.orders.Find(x => x.item.itemId == item.itemId);
            if (orderToUpdate != null && orderToUpdate.quantityRecieved < orderToUpdate.quantity)
            {
                orderToUpdate.quantityRecieved++;// = orderToUpdate.quantity--;
                onCorrectItemPlaced?.Invoke(model.orders.IndexOf(orderToUpdate), orderToUpdate);
            }
            else
            {
                // placed wrong item
                onWrongItemPlaced?.Invoke(item);
            }
            if (isOrderSuccess = CheckOrderCompleted())
            {
                model.customerState = CustomerState.IDLE;
                GameManager.instance.players[model.occupiedPlayerId].OnCompletsOrder(model,true);
                Reset();
            }
        }

        /// <summary>
        /// fetching this customers current state
        /// </summary>
        /// <returns></returns>
        public CustomerState GiveMeCustomerState()
        {
            return model.customerState;
        }

        /// <summary>
        /// Checking whether all items prepared
        /// </summary>
        /// <returns></returns>
        bool CheckOrderCompleted()
        {
            var checkingOrder = model.orders.Find(x => x.quantityRecieved < x.quantity);
            return checkingOrder == null;
        }
        
        public Customer GetCustomer()
        {
            return model;
        }
    }
}
