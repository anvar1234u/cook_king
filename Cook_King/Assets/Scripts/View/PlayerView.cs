﻿using System;
using System.Collections;
using System.Collections.Generic;
using CookKing.MVC.Controller;
using CookKing.MVC.Model;
using CookKing.UI;
using CookKing.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace CookKing.MVC.View
{
    [Serializable]
    public class PlayerView
    {
        #region inspector_fields
        [SerializeField] private Text playerNameText,scoreText,orderCountText,timerText;
        [SerializeField] private Image avatar;
        [SerializeField] private Plate plate;
        #endregion

        private Player playerModel;
        public bool isCustomerIsThanking = false;

        public void InitView(Player model)
        {
            playerModel = model;
            playerNameText.text = playerModel.playerName;
            scoreText.text = "Score : 0";
            orderCountText.text = "Orders : 0";
            timerText.text = "Time : "+GameManager.instance.GiveTimeString(0);
            avatar.sprite = playerModel.avatar;
            GameManager.instance.onTimerUpdated += UpdateTimeText;
        }

        public void UpdateScoreView()
        {
            scoreText.text = "Score : "+playerModel.score;
            orderCountText.text = "Orders : "+playerModel.orderCompleted;
        }

        void UpdateTimeText(string timer)
        {
            timerText.text = timer;
        }

        /// <summary>
        /// when player selects a customer have to enable plate
        /// </summary>
        public void OnSelectCustomer(Customer customer)
        {
            plate.customerAvatar.sprite = customer.avatar;
            plate.plateObj.SetActive(true);
        }

        /// <summary>
        /// Enable the plate object Set the order place holders;
        /// </summary>
        public void SetPlateView(int index, Order order)
        {
            var orderPlaceHolder = plate.orderPlaceHolders[index];
            orderPlaceHolder.itemIcon.sprite = order.item.icon;
            orderPlaceHolder.itemCount.text = order.quantityRecieved.ToString();
            orderPlaceHolder.gameObject.SetActive(true);
        }

        /// <summary>
        /// Customers response will be shown in speech bubble
        /// </summary>
        /// <param name="message"></param>
        public void SetSpeechBubble(string message,Action onSpeechOver=null)
        {
            plate.chatText.gameObject.SetActive(false);
            plate.chatText.text = message;
            plate.speechBubble.color = new Color(1, 1, 1, 0);
            LeanTween.cancel(plate.speechBubble.rectTransform);
            plate.speechBubble.gameObject.SetActive(true);
            LeanTween.alpha(plate.speechBubble.rectTransform, 1, 0.1f).setOnComplete(() =>
            {
                plate.chatText.gameObject.SetActive(true);
                LeanTween.alpha(plate.speechBubble.rectTransform, 0, 0.1f).setDelay(2).setOnComplete(()=> {
                    plate.speechBubble.gameObject.SetActive(false);
                    plate.chatText.gameObject.SetActive(false);
                    onSpeechOver?.Invoke();
                });
               
            });
        }

        public void ShowSpeechBubbleOnOrderFinish(bool isSuccess)
        {
            isCustomerIsThanking = true;
            if (isSuccess)
            {
                SetSpeechBubble("Thank you!!", ResetPlateView);
            }
            else
            {
                SetSpeechBubble("I Can't wait any more", ResetPlateView);
            }
        }

        /// <summary>
        /// on order finshes have to disable plate, order can be success or fail
        /// </summary>
        public void ResetPlateView()
        {
            plate.plateObj.SetActive(false);
            isCustomerIsThanking = false;
            playerModel.isSelectedCustomerPanel = true;
            foreach (OrderView orderView in plate.orderPlaceHolders)
            {
                orderView.gameObject.SetActive(false);
            }
            LeanTween.cancel(plate.speechBubble.rectTransform);
        }


    }
    [Serializable]
    public class Plate
    {
        public GameObject plateObj;
        public Image customerAvatar;
        public Image speechBubble;
        public Text chatText;
        public List<OrderView> orderPlaceHolders;
    }
}
