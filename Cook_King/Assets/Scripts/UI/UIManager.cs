﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace CookKing.UI
{
    public class UIManager : MonoBehaviour
    {
        public static UIManager instance;

        public ResultScreen resultScreen;
        public GameObject gamePlayScreen;

        public Transform trash;
        [SerializeField] private Animator trashAnimator;
        // Start is called before the first frame update
        void Awake()
        {
            instance = this;
        }
        public void PlayTrashAnimation()
        {
            trashAnimator.SetTrigger("Shake");
        }
    }
}