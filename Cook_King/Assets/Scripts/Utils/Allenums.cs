﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace CookKing.Utils
{
    public enum Actions
    {
        MOVE_RIGHT,
        MOVE_LEFT,
        SELECT
    }
    public enum CustomerState
    {
        IDLE,
        WAITING,
        OCCUPIED
    }
    public enum Result
    {
        WIN,
        LOSE,
        TIE
    }

}