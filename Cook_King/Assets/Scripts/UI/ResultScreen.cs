﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using CookKing.Utils;
using CookKing.MVC.Model;
using UnityEngine.SceneManagement;

namespace CookKing.UI
{
    public class ResultScreen : MonoBehaviour
    {
        [SerializeField] private List<PlayerResultView> playerResultViews;

        const string COLON = "   :    ";


        public void Activate()
        {
            CustomerSlotManager.instance.Reset();
            UIManager.instance.gamePlayScreen.SetActive(false);
            gameObject.SetActive(true);
            SetView();
        }

        void Deactivate()
        {
            gameObject.SetActive(false);
        }

        public void PlayAgainButtonClicked()
        {
            for (int i = 0; i < GameManager.instance.players.Count; i++)
            {
                GameManager.instance.players[i].Reset();
            }
            GameManager.instance.PlayGame();
            Deactivate();
        }

        public void PlayNewGameButtonClicked()
        {
            SceneManager.LoadScene("MainScene");
        }


        private void SetView()
        {
            for (int i = 0; i < GameManager.instance.players.Count; i++)
            {
                var _playerResultView = playerResultViews[i];
                var playerModel = GameManager.instance.players[i]._model;
                _playerResultView.avatar.sprite = playerModel.avatar;
                _playerResultView.scoreText.text = COLON + playerModel.score;
                _playerResultView.playerName.text =  playerModel.playerName;
                _playerResultView.resultText.text = GiveMeResultString(playerModel.result);
                _playerResultView.orderCompletdCountText.text = COLON + playerModel.orderCompleted;
                _playerResultView.orderFailedCountText.text = COLON + playerModel.orderFailed;
                _playerResultView.orderTotalCountText.text = COLON + playerModel.totalOrderAttended;
                _playerResultView.orderAccuracy.text = COLON +Mathf.RoundToInt(GiveMeOrderAccuracy(playerModel))+ " %";
                _playerResultView.itemCompletdCountText.text = COLON + playerModel.itemServed;
                _playerResultView.itemTrashedCountText.text = COLON + playerModel.itemTrashed;
                _playerResultView.itemChoppedTotalCountText.text = COLON + playerModel.totalItemsChopped;
                _playerResultView.choppingAccuracy.text = COLON + Mathf.RoundToInt(GiveMeChoppingAccuracy(playerModel)) + " %";
            }
        }

        string GiveMeResultString(Result result)
        {
            string res = "";
            switch (result)
            {
                case Result.LOSE:
                    res = "YOU LOST!";
                    break;
                case Result.WIN:
                    res = "YOU WON!";
                    break;
                case Result.TIE:
                    res = "DRAW!";
                    break;
            }
            return res;
        }

        float GiveMeOrderAccuracy(Player player)
        {
            if (player.totalOrderAttended <= 0) return 0;
            return ((float)player.orderCompleted /(float) player.totalOrderAttended)*100f;
        }
        float GiveMeChoppingAccuracy(Player player)
        {
            if (player.totalItemsChopped <= 0) return 0;
            return ((float)player.itemServed  / (float)player.totalItemsChopped)*100f;
        }

    }
}

