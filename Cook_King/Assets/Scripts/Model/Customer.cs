﻿using System;
using System.Collections.Generic;
using CookKing.Utils;
using UnityEngine;
namespace CookKing.MVC.Model
{
    [Serializable]
    public class Customer
    {
        public string cusName;
        public Sprite avatar;
        public List<Order> orders = new List<Order>();
        public CustomerState customerState;
        public int waitingTime;//in seconds
        public int occupiedPlayerId = -1;
    }
}
