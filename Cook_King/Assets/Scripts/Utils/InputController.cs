﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace CookKing.Utils
{
    public class InputController : MonoBehaviour
    {

        public static Action<string> onKeyPressed;

        // Update is called once per frame
        void Update()
        {
            if (Input.anyKeyDown)
            {
                onKeyPressed?.Invoke(Input.inputString.ToUpper());
            }
        }
    }
}
