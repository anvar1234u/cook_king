﻿using System;
using System.Collections.Generic;
using CookKing.Utils;
using UnityEngine;
namespace CookKing.MVC.Model
{
    [Serializable]
    public class Player
    {
        public string playerName;
        public int playerId;
        public Sprite avatar;
        public int orderCompleted;
        public int orderFailed;
        public int totalOrderAttended;
        public int itemServed;
        public int itemTrashed;
        public int totalItemsChopped;
        public int score;
        public List<PlayerInputKeys> playerInputKeys;// this is for inspector assignments
        public Dictionary<string, Actions> inputKeys = new Dictionary<string, Actions>();// this is for easy access
        public bool isPrimaryBoardOccupied;
        public bool isSecondaryBoardOccupied;
        public bool isSelectedCustomerPanel = true;// selection flag says inventory selection or customer selection
        public Result result;

        public void Reset()
        {
            orderFailed = 0;
            orderCompleted = 0;
            totalOrderAttended = 0;
            itemServed = 0;
            itemTrashed = 0;
            totalItemsChopped = 0;
            score = 0;
            isPrimaryBoardOccupied = false;
            isSecondaryBoardOccupied = false;
            isSelectedCustomerPanel = true;
        }

    }
    [Serializable]
    public class PlayerInputKeys
    {
        public string key;
        public Actions action;
    }
}