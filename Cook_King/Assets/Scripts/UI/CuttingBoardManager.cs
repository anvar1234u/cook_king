﻿using System.Collections;
using System.Collections.Generic;
using CookKing.MVC.Model;
using UnityEngine;
using UnityEngine.UI;
using System;
using CookKing.Utils;

namespace CookKing.UI
{
    public class CuttingBoardManager : MonoBehaviour
    {
        #region inspector_fields
        [SerializeField] private GameObject sliderObject;
        [SerializeField] private Image cuttingTimeSlider;
        [SerializeField] private List<Image> placeHolders;// secondaryBoardPlaceHolder;
        [SerializeField] private Image imageToShowTrashAnimation;
        #endregion
        List<Item> itemQueue = new List<Item>();
        public Action<Item,int> OnPreperationCompleted;
        // Start is called before the first frame update
        void Start()
        {

        }

        public void PlaceTheItemOnBoard(Item item)
        {
            //if (isPrimary)
            //{
            //    primaryBoardPlaceHolder.gameObject.SetActive(true);
            //    primaryBoardPlaceHolder.sprite = item.icon;
            //}
            //else
            //{
            //    secondaryBoardPlaceHolder.gameObject.SetActive(true);
            //    secondaryBoardPlaceHolder.sprite = item.icon;
            //}
            //Debug.Log(index);
            if (itemQueue.Count == 0)
            {
                placeHolders[0].gameObject.SetActive(true);
                itemQueue.Add(item);
                StopAllCoroutines();
                StartCoroutine(RunCuttingTimer(item));
            }
            else
            {
                if (itemQueue.Count == 2)
                {
                    itemQueue.RemoveAt(1);
                }
                itemQueue.Add(item);
                placeHolders[1].gameObject.SetActive(true);
                placeHolders[1].sprite = item.icon;
            }
            Debug.Log("itemQueue count " + itemQueue.Count);
            //Debug.Log(itemQueue.Count);
        }


        IEnumerator RunCuttingTimer(Item itemToPrepare)
        {
            foreach (Image image in placeHolders)
            {
                image.gameObject.SetActive(false);
            }
            placeHolders[0].sprite = itemToPrepare.icon;
            placeHolders[0].gameObject.SetActive(true);
            //if (itemQueue.Count > 0)
            //{
            //    Debug.Log("itemQueue count " + itemQueue.Count);
            //}
            //else
            //{
            //    yield return null;
            //}
            sliderObject.SetActive(true);
            cuttingTimeSlider.fillAmount = 1;
            var tempVal = (float)itemToPrepare.preperationTime;
            while (tempVal > 0)
            {
                var deltaTime = Time.deltaTime;
                tempVal -= deltaTime;
                cuttingTimeSlider.fillAmount =(float) tempVal/ itemToPrepare.preperationTime;
                yield return new WaitForSeconds(deltaTime);
            }
            sliderObject.SetActive(false);
            placeHolders[0].gameObject.SetActive(false);
            itemQueue.RemoveAt(0);
            OnPreperationCompleted?.Invoke(itemToPrepare, itemQueue.Count);
            if (itemQueue.Count > 0)
            {
                StartCoroutine(RunCuttingTimer(itemQueue[0]));
            }
        }

        /// <summary>
        /// when chop a wrong item it will Trash automatically, (plays animation)
        /// </summary>
        public void TrashTheItem(Item item)
        {
            imageToShowTrashAnimation.sprite = item.icon;
            imageToShowTrashAnimation.gameObject.SetActive(true);
            LeanTween.move(imageToShowTrashAnimation.gameObject, UIManager.instance.trash, 1).setOnComplete(()=>{
                imageToShowTrashAnimation.gameObject.SetActive(false);
                imageToShowTrashAnimation.transform.position = placeHolders[0].transform.position;
                UIManager.instance.PlayTrashAnimation();
            });
        }

        public void Reset()
        {
            StopAllCoroutines();
            itemQueue.Clear();
            placeHolders[0].gameObject.SetActive(false);
            placeHolders[1].gameObject.SetActive(false);
            sliderObject.SetActive(false);
        }

        void OnDeleteAnItem()
        {
        }


    }
}
