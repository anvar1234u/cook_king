﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using CookKing.MVC.Controller;
using CookKing.MVC.Model;
using CookKing.Utils;
using System.Collections;

namespace CookKing.MVC.View
{
    [Serializable]
    public class CustomerView
    {
        #region Inspector_fields
        [SerializeField] private List<OrderView> orders;
        [SerializeField] private GameObject descripTionObj;
        [SerializeField] private Image waitinTimeSlider;
        [SerializeField] private Text waitingTime;
        [SerializeField] private Image border;
        
        #endregion

        /// <summary>
        /// Initialize Customer UI view and activates
        /// </summary>
        //public void Init()
        //{
        //    gameObject.SetActive(true);
        //    _controller.CreateOrder();
        //}

        /// <summary>
        /// when a customer creates an order need to update that in view
        /// </summary>
        /// <param name="customer"></param>
        public void SetView(Customer customer)
        {
            DisbaleAllOrderViews();
            foreach (Order order in customer.orders)
            {
                var index = customer.orders.IndexOf(order);
                orders[index].SetView(order);
            }
            waitinTimeSlider.fillAmount = 1;
            waitingTime.text = GameManager.instance.GiveTimeString(customer.waitingTime);
        }

        /// <summary>
        /// Disble and reset old orders
        /// </summary>
        private void DisbaleAllOrderViews()
        {
            foreach (OrderView orderView in orders)
            {
                orderView.Reset();
            }
        } 
        /// <summary>
        /// Called when order is completed it can be success or time out.
        /// </summary>
        public void ResetView()
        {
            waitinTimeSlider.gameObject.SetActive(false);
            ResetNavigation();
        }

        
        /// <summary>
        /// on player selects or conforms an order 
        /// </summary>
        public void OccupyThisCustomer()
        {
            descripTionObj.SetActive(false);
            border.color = Color.black;
            waitinTimeSlider.gameObject.SetActive(true);
            waitinTimeSlider.fillAmount = 1;
        }

       
        
        /// <summary>
        /// Show the remaining time in the view
        /// </summary>
        /// <param name="val"></param>
        public void SetTimeSlider(float val)
        {
            waitinTimeSlider.fillAmount = val;
        }

        /// <summary>
        /// to indicate the navigation through the customer slot
        /// </summary>
        public void Navigate(int playerId,bool isOnSameCustomer=false)
        {
            descripTionObj.SetActive(true);
            border.color = playerId == 0?Color.red:Color.blue;//customerModel.navigationColor;
            if (isOnSameCustomer)
            {
                border.color = Color.red + Color.blue;
            }
        }

        /// <summary>
        /// to reset the navigation according to input
        /// </summary>
        public void ResetNavigation()
        {
            descripTionObj.SetActive(false);
            if (border.color != Color.green)
            {
               border.color = Color.black;
            }
        }
    }
    
}