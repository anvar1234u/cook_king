Cook-KING
	- a basic salad simulation game

Prerequisites
	- Unity 2019.2.9f1
	
Testing
	- two player game 
	- first player controls
		- A : prev/move left
		- D : next/move right
		- S : Select
	- Second player controls
		- J : prev/move left
		- L : next/move right
		- K : Select
	- Select any customer from the list using controls (player 1 red, player 2 blue)
	- now the selection focus is on inventory, select the items and serve
	- two cutting borads are there for each player, second one is buffer, we can change the buffer item at any time, but we can not change the main item
	- Wrong item will trash automatically
	- Result will show once the game time ends
	
Future enhancement
	- Audios
	- We can add time boosters
	- Power ups
	- Customize controls
