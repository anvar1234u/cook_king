﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace CookKing.MVC.Model
{
    [CreateAssetMenu(fileName = "Models", menuName = "Item")]
    public class Item : ScriptableObject
    {
        public int itemId;
        public string itemName;
        public Sprite icon;
        public int preperationTime;//in seconds
    }
}
