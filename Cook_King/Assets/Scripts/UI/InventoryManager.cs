﻿using System;
using System.Collections;
using System.Collections.Generic;
using CookKing.MVC.Controller;
using CookKing.MVC.Model;
using CookKing.MVC.View;
using CookKing.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace CookKing.UI
{
    public class InventoryManager : MonoBehaviour
    {
        #region inspector_fields
        [SerializeField] private GameObject itemDescriptionObj;
        [SerializeField] private Text itemNameText, itemDescriptionText;
        [SerializeField] private GameObject itemPrefab;
        #endregion
        int navigationIndex = 0;
        private List<ItemController> itemViews = new List<ItemController>();
        ItemController previousObject;
        public Action<Item> onSelectItem;
        // Start is called before the first frame update
        void Start()
        {
            var items = GameManager.instance.allItems;
            foreach (Item item in items)
            {
                var obj = Instantiate(itemPrefab, transform);
                var itemController = obj.GetComponent<ItemController>();
                itemController.Init(item);
                itemViews.Add(itemController);
            }
        }

        /// <summary>
        /// handles the players input based on player id (0 and 1)
        /// </summary>
        /// <param name="action"></param>
        /// <param name="player"></param>
        internal void PlayerInput(Actions action)
        {
            
            switch (action)
            {
                case Actions.MOVE_LEFT:
                    navigationIndex = navigationIndex <= 0 ? itemViews.Count - 1 : navigationIndex - 1;
                    UpdateNavigation();
                    break;
                case Actions.MOVE_RIGHT:
                    navigationIndex = navigationIndex >= itemViews.Count - 1 ? 0 : navigationIndex + 1;
                    UpdateNavigation();
                    break;
                case Actions.SELECT:
                    UpdateSelection();
                    break;

            }

        }

        private void UpdateSelection()
        {
            var selectedItem = itemViews[navigationIndex];
            onSelectItem?.Invoke(selectedItem.model);
 
        }

        public void UpdateNavigation()
        {
            var item = itemViews[navigationIndex];
            item.view.Select();
            if (previousObject != null)
            {
                previousObject.view.DeSelect();
            }
            previousObject = item;
            itemDescriptionObj.SetActive(true);
            itemNameText.text = item.model.name;
            itemDescriptionText.text = "Chopping time : " + item.model.preperationTime + "s";//GameManager.instance.GiveTimeString(item.model.preperationTime);
        }

        public void Reset()
        {
            navigationIndex = 0;
            itemDescriptionObj.SetActive(false);
            if (previousObject != null)
                previousObject.view.DeSelect();
            previousObject = null;
        }

    }
}
