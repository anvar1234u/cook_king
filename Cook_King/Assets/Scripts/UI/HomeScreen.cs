﻿using System.Collections;
using System.Collections.Generic;
using CookKing.Utils;
using UnityEngine;
using UnityEngine.UI;
namespace CookKing.UI
{
    public class HomeScreen : MonoBehaviour
    {
        // Start is called before the first frame update

        [SerializeField] private Image selectedAvatar;
        [SerializeField] private InputField nameField, moveLeftFeild, moveRightFeild, selectField;
        [SerializeField] private Text titleText;
        int playerReadyCount = 0;

        void Start()
        {
            InitInputFields();
            titleText.text = "Player 1 Setup";
        }

        public void DoneButtonClicked()
        {
            if (playerReadyCount >= 1)
            {
                //player 2 setup
               
                var player2 = GameManager.instance.players[playerReadyCount]._model;
                player2.playerName = string.IsNullOrEmpty(nameField.text)?"Player2":nameField.text;
                player2.avatar = selectedAvatar.sprite;
                gameObject.SetActive(false);
                GameManager.instance.PlayGame();
            }
            else
            {
                //player 1 setup
                var player1 = GameManager.instance.players[playerReadyCount]._model;
                player1.playerName = string.IsNullOrEmpty(nameField.text) ? "Player1" : nameField.text;
                player1.avatar = selectedAvatar.sprite;
                titleText.text = "Player 2 Setup";
                playerReadyCount++;
                InitInputFields();
            }
        }
        public void OnSelectAvatar(Image image)
        {
            selectedAvatar.sprite = image.sprite;
            //GameManager.instance.players[playerReadyCount]._model.avatar = image.sprite;
        }

        void InitInputFields()
        {
            var player = GameManager.instance.players[playerReadyCount]._model;
            nameField.text = "Player"+(playerReadyCount+1);
            moveLeftFeild.text = player.playerInputKeys.Find(x => x.action == Actions.MOVE_LEFT).key.ToUpper();
            moveRightFeild.text = player.playerInputKeys.Find(x => x.action == Actions.MOVE_RIGHT).key.ToUpper();
            selectField.text = player.playerInputKeys.Find(x => x.action == Actions.SELECT).key.ToUpper();
        }
    }
}
