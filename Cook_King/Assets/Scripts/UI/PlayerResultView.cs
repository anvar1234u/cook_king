﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace CookKing.UI
{
    public class PlayerResultView : MonoBehaviour
    {

        public Text resultText;
        public Text playerName;
        public Image avatar;
        public Text scoreText;
        public Text orderCompletdCountText;
        public Text orderFailedCountText;
        public Text orderTotalCountText;
        public Text orderAccuracy;
        public Text itemCompletdCountText;
        public Text itemTrashedCountText;
        public Text itemChoppedTotalCountText;
        public Text choppingAccuracy;

    }
}
