﻿using System;
using System.Collections.Generic;
using CookKing.MVC.Controller;
using CookKing.MVC.Model;
using UnityEngine;
using UnityEngine.UI;
namespace CookKing.MVC.View
{
    [Serializable]
    public class ItemView
    {
        #region inspector_fields
        [SerializeField] private Image imageIcon;
        [SerializeField] private GameObject border;
        #endregion

        public void Init(Item item)
        {
            imageIcon.sprite = item.icon;
            border.SetActive(false);
        }

        public void Select()
        {
            border.SetActive(true);
        }

        public void DeSelect()
        {
            border.SetActive(false);
        }
    }
}
